const https = require("https");
const caldavEdith = require(".");

function cal(req, res) {

	/* Convert authorization header from ISO-8859-1 to UTF-8 */
	if (req.headers.authorization) {
		var auth = Buffer.from(req.headers.authorization.substr(6), "base64").toString("latin1");
		req.headers.authorization = "Basic " + Buffer.from(auth).toString("base64");
	}

	if (req.url === "/favicon.ico")
		res.end();
	else if (req.url.startsWith("/infcloud/auth")) {
		auth = auth.split(":");
		res.setHeader("content-type", "text/xml");
		res.end(`<?xml version="1.0" encoding="utf-8"?>
			<resources xmlns="urn:com.inf-it:configuration">
				<resource>
					<type>
						<calendar/>
					</type>
					<href>http://${req.headers.host}/${auth[0]}/</href>
					<checkcontenttype>true</checkcontenttype>
					<userauth>
						<username>${auth[0]}</username>
						<password>${auth[1]}</password>
					</userauth>
					<timeout>90000</timeout>
					<locktimeout>10000</locktimeout>
				</resource>
			</resources>
		`);
	} else if (req.url.startsWith("/infcloud")) {
		delete req.headers.host;
		var Req = https.request("https://inf-it.com" + req.url, {
			method: req.method,
			headers: req.headers
		}).on("response", function(r) {
			res.writeHead(r.statusCode, r.statusMessage, r.headers);
			r.pipe(res);
		});
		req.on("close", function() {
			if (!this.complete && Req) Req.destroy();
		});
		res.on("close", function() {
			if (!this.writableEnded && Req) Req.destroy();
		});
		req.pipe(Req);
	} else {
		res.on("finish", function() {
			console.log(req.method, decodeURI(req.url), res.statusCode, res.statusMessage);
		});
		caldavEdith(req, res);
	}
}

module.exports = cal;
