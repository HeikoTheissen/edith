const Koa = require("koa");
const compress = require("koa-compress");
const adapter = require("caldav-adapter");
const moment = require("moment");
const unescape = require("unescape-html");
const { session, authenticate, request, search } = require("../request");

var app = new Koa();
var cals = {};
var orte = new Map();
var veranst = new Map();
var cats = new Map();

function changedate(date, time) {
	return (
		date.replace(/^(\d\d)\.(\d\d)\.(\d\d\d\d)$/, "$3$2$1") +
		"T" +
		time.replace(":", "") +
		"00"
	);
}

function requestOptions(principalId, method) {
	return {
		method,
		headers: {
			"accept-encoding": "br,gzip,deflate",
			cookie: session[principalId].cookie
		}
	};
}

function getEventsByDate({ calendarId, principalId, start, end }) {
	return request(
		`/ekiba/ajax/d/3431?action=dtGetRecords&con=veranst`,
		requestOptions(principalId, "POST"),
		{
			...search(
				12,
				`${moment(start)._d.toLocaleDateString()}-${moment(end)._d.toLocaleDateString()}`
			),
			...search(17, calendarId)
		}
	).then(function ({ body }) {
		var events = [];
		JSON.parse(body).data.forEach(function (event) {
			var dt;
			var id = event[3].match(/href="(\d+)\?tid=(\d+)"/);
			id[0] = "edith6";
			id[3] = event[1];
			id[4] = veranst.get(calendarId);
			id[5] = cats.get(event[11].split(",", 1)[0]);
			var eventId = id.join("-");
			var e = (cals[calendarId].events[eventId] = {
				eventId: eventId,
				url: "urn:" + eventId,
				calendarId: calendarId,
				summary: event[8],
				location: event[15].match(/<\/i>\s*(.*)/)?.[1] || "",
				createdOn: changedate(event[28], event[29]),
				lastModifiedOn: changedate(event[30], event[31])
			});
			if (
				(dt = event[12].match(
					/(\d\d)\.(\d\d)\.(\d\d\d\d),\s+(\d\d):(\d\d).*?(\d\d)\.(\d\d)\.(\d\d\d\d),\s+(\d\d):(\d\d)/
				))
			) {
				e.startDate = dt[3] + dt[2] + dt[1] + "T" + dt[4] + dt[5] + "00";
				e.endDate = dt[8] + dt[7] + dt[6] + "T" + dt[9] + dt[10] + "00";
			} else if (
				(dt = event[12].match(
					/(\d\d)\.(\d\d)\.(\d\d\d\d),\s+((\d\d):(\d\d)\s+((\d\d):(\d\d))?)?/
				))
			) {
				e.startDate = dt[3] + dt[2] + dt[1];
				if (dt[4]) {
					e.startDate += "T" + dt[5] + dt[6] + "00";
					if (dt[7])
						e.endDate = dt[3] + dt[2] + dt[1] + "T" + dt[8] + dt[9] + "00";
					else e.endDate = moment(e.startDate).add(1, "h")._i;
				} else {
					e.startDate += "T000000";
					e.endDate = moment(e.startDate).add(1, "d")._i;
				}
			} else return;
			events.push(e);
		});
		return events;
	});
}

function updateEvent({ event, calendarId, principalId }) {
	var p;
	if (!event.url) event.url = "urn:" + event.eventId;
	var id = event.url.split("-");
	if (id[0] !== "urn:edith6") return Promise.reject("Not supported");
	if (event.recurring) return Promise.reject("Recurring events not supported");
	var start = moment(event.startDate);
	var end = moment(event.endDate);
	var payload = {
		beginn: start._d.toLocaleDateString(),
		uhrzeit_von: start.hour() + ":" + start.minute(),
		ende: end._d.toLocaleDateString(),
		uhrzeit_bis: end.hour() + ":" + end.minute(),
		fk_t125_oertlichkeit0: orte.get(event.location || "\u00a0"),
		anreisser_abweichend: event.description || "",
		anreisser_abweichend_jn: "t",
		memos_ersetzend_jn: "t",
		abweichungen_basis_jn: "t"
	};
	payload["fk_t125_oertlichkeit_n_orte[]"] = payload.fk_t125_oertlichkeit0;
	if (id[3] === "0") {
		payload.formularName = "termine_bearbeiten";
		payload.veranst_sys_pk = id[1];
		payload.termin_sys_pk = id[2];
		if (id[2] === "0") payload.fuegeTerminHinzu = "Hinzufügen";
		else {
			payload.speichereTermin = "Speichern";
		}
		payload.typ = 1;
		payload.termin_level = 2;
		payload["sichtbarkeit[veranstaltungskalender][sys_pk]"] = 1;
		payload.sichtbar_jn = "t";
		p = request(
			`/ekiba/edith/elem/3427/${payload.veranst_sys_pk}?tab=${payload.formularName}&scene=${payload.formularName}`,
			requestOptions(principalId, "POST"),
			payload
		);
	} else {
		payload.formularName = "einzeltermin_bearbeiten";
		payload.fk_t125_veranstaltung = id[1];
		payload.fk_serientermine_main = id[2];
		payload.serien_termin_sys_pk = id[3];
		payload.fk_t125_status = 3;
		payload.termin_level = 3;
		payload.speichereEintrag = "Speichern";
		p = request(
			`/ekiba/edith/quick/3432/${payload.serien_termin_sys_pk}?tab=${payload.formularName}&scene=${payload.formularName}`,
			requestOptions(principalId, "POST"),
			payload
		);
	}
	return p.then(function ({ statusCode, statusMessage }) {
		if (statusCode < 400)
			return (cals[calendarId].events[event.eventId] = event);
		else throw statusMessage;
	});
}

app.use(compress());
app.use(
	adapter.koa({
		authenticate: function ({ username, password }) {
			return authenticate(username, password).then(function () {
				return {
					principalId: username,
					principalName: username
				};
			});
		},
		authRealm: "EDITH 6.0",
		proId: { company: "EDITH 6.0", product: "Kalender", language: "DE" },
		data: {
			getCalendar({ calendarId, principalId }) {
				return Promise.resolve(cals[calendarId]);
			},
			getCalendarsForPrincipal({ principalId }) {
				function masterData(col, m) {
					return request(
						`/ekiba/ajax/d/3431?action=dtGetColFilter&con=veranst&col=${col}`,
						requestOptions(principalId, "GET")
					).then(function ({ body }) {
						JSON.parse(body).results.forEach(function ({ id, name }) {
							m.set(name, id);
						});
					});
				}

				return Promise.all([
					masterData(10, cats),
					masterData(15, orte),
					masterData(17, veranst)
				]).then(function () {
					var calendars = [];
					for (var [name, id] of veranst)
						calendars.push(
							(cals[id] = {
								calendarId: String(id),
								calendarName: name,
								timeZone: "Europe/Berlin",
								events: {}
							})
						);
					return calendars;
				});
			},
			getEventsForCalendar({ calendarId, principalId }) {
				return getEventsByDate({
					calendarId,
					principalId,
					start: "00010101",
					end: "99991231"
				});
			},
			getEventsByDate,
			getEvent({ eventId, calendarId, principalId }) {
				return Promise.resolve(cals[calendarId].events[eventId]);
			},
			createEvent({ event, calendarId, principalId }) {
				var p;
				var id = event.eventId.split("-");
				if (
					id[0] === "edith6" &&
					cals[calendarId].events[event.eventId].summary === event.summary
				)
					p = Promise.resolve();
				else {
					var payload = {
						formularName: "terminpflege_allgemein",
						fk_t125_terminarten: 1458,
						fk_t171_kalender: calendarId,
						fk_t125_veranstalter:
							id[0] === "edith6" ? id[4] : veranst.values().next().value,
						titel: event.summary,
						fk_t125_kategorie:
							id[0] === "edith6" ? id[5] : cats.values().next().value,
						fk_t125_oertlichkeit0: orte.get(event.location || "\u00a0"),
						sichtbar_jn: "t",
						fuegeVeranstaltungHinzu: "Hinzufügen"
					};
					payload["fk_t125_oertlichkeit_n_orte[]"] =
						payload.fk_t125_oertlichkeit0;
					p = request(
						`/ekiba/edith/elem/3427/-1`,
						requestOptions(principalId, "POST"),
						payload
					).then(function ({ statusCode, statusMessage, headers }) {
						if (statusCode < 400) {
							var tid = headers.location;
							tid = tid && tid.match(/\/ekiba\/edith\/elem\/3427\/(.*?)\?/);
							tid = tid && tid[1];
							if (tid) id = ["edith6", tid];
							else throw "Event creation failed";
						} else throw statusMessage;
					});
				}
				return p.then(function () {
					id[2] = id[3] = "0";
					event.url = "urn:" + id.join("-");
					return updateEvent({ event, calendarId, principalId });
				});
			},
			updateEvent,
			deleteEvent({ eventId, calendarId, principalId }) {
				var event = cals[calendarId].events[eventId];
				if (!event)
					return Promise.reject("Event " + eventId + " does not exist");
				var id = event.url.split("-");
				if (id[0] !== "urn:edith6" || id[3] !== "0")
					return Promise.reject("Not supported");
				var payload = {
					formularName: "termine_bearbeiten",
					veranst_sys_pk: id[1],
					termin_sys_pk: id[2],
					typ: 1,
					loescheTerminAusListe: 1
				};
				return request(
					`/ekiba/edith/elem/3427/${
						payload.veranst_sys_pk
					}?tab=termine_bearbeiten&scene=${payload.formularName}`,
					requestOptions(principalId, "POST"),
					payload
				).then(function ({ statusCode, statusMessage }) {
					if (statusCode < 400) delete cals[calendarId].events[eventId];
					else throw statusMessage;
				});
			}
		}
	})
);

module.exports = app.callback();
