const https = require("https");
const zlib = require("zlib");
const readline = require("readline");
const { Writable } = require("stream");

var agent = new https.Agent({ maxSockets: 10 });
var session = {};

class UserManager {
	getDefaultUser(callback) {
		callback(null);
	}
	getUserByNamePassword(name, password, callback) {
		return authenticate(name, password).then(function () {
			callback.bind(null, { username: name });
		}, callback);
	}
}

function credentials({ stdin, stdout }) {
	var muted;
	return new Promise(function (resolve, reject) {
		var rl = readline.createInterface({
			input: stdin,
			output: new Writable({
				write(chunk, encoding, callback) {
					if (!muted || chunk.toString() === "\r\n") stdout.write(chunk);
					callback();
				}
			}),
			terminal: true
		});
		rl.question("Benutzer: ", function (username) {
			rl.question("Passwort: ", function (password) {
				rl.close();
				authenticate(username, password).then(function () {
					resolve(username);
				}, reject);
			});
			muted = true;
		});
	});
}

function authenticate(username, password) {
	return session[username] && session[username].password === password
		? Promise.resolve()
		: request(
				"/ekiba/session/login",
				{
					method: "POST",
					headers: {
						"content-type": "application/x-www-form-urlencoded"
					}
				},
				{ account: username, password, doFormLogin: 1 }
			).then(function ({ statusCode, headers }) {
				if (statusCode === 302) {
					session[username] = {
						cookie: headers["set-cookie"][0].match(/^(ekiba=.*?);/)[1],
						password: password
					};
					return Promise.all([
						request(`/ekiba/edith/elem/2262`, {
							headers: {
								cookie: session[username].cookie
							}
						}).then(function ({ body }) {
							session[username].user = JSON.parse(
								Buffer.from(
									body.match(
										/<input type="hidden" name="old_values_.*?" value="(.*?)"/
									)[1],
									"base64"
								)
							).sys_pk;
						}),
						request(
							`/ekiba/ajax/d/2856?con=aktuelluebersicht&action=setDtFilterOldEntries&val=0&reqUrl=/ekiba/edith/elem/2856`,
							{
								headers: {
									cookie: session[username].cookie
								}
							}
						)
					]);
				} else {
					delete session[username];
					throw "Login failed";
				}
			});
}

function request(url, options, body) {
	if (typeof body === "object" && !(body instanceof Buffer)) {
		options.headers["Content-Type"] = "application/x-www-form-urlencoded";
		body = new URLSearchParams(body).toString();
	}
	return new Promise(function (resolve, reject) {
		var req = https
			.request(
				"https://lukas.edith-cms.de" + url,
				Object.assign({ agent }, options)
			)
			.on("response", function (r) {
				var out;
				switch (r.headers["content-encoding"]) {
					case "br":
						out = r.pipe(zlib.createBrotliDecompress());
						break;
					case "gzip":
						out = r.pipe(zlib.createGunzip());
						break;
					case "deflate":
						out = r.pipe(zlib.createInflate());
						break;
					case undefined:
					case "identity":
						out = r;
						break;
					default:
						throw (
							"Unsupported Content-Encoding " + r.headers["content-encoding"]
						);
				}
				var body = "";
				out
					.on("data", function (chunk) {
						body += chunk.toString();
					})
					.on("end", function () {
						resolve({
							statusCode: r.statusCode,
							statusMessage: r.statusMessage,
							headers: r.headers,
							body: body
						});
					});
			})
			.on("error", reject);
		req.end(body);
	});
}

function search(pos, value) {
	var payload = {};
	payload[`columns[${pos}][data]`] = pos;
	payload[`columns[${pos}][searchable]`] = "true";
	payload[`columns[${pos}][search][value]`] = value;
	payload[`columns[${pos}][search][regex]`] = "false";
	return payload;
}

module.exports = {
	UserManager,
	session,
	credentials,
	authenticate,
	request,
	search
};
