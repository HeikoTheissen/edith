const webdav = require("webdav-server").v2;
const { PassThrough, Writable } = require("stream");
const { session, credentials, request, search } = require("../request");
const { getType } = require("mime");
const escape = require("escape-html");
const unescape = require("unescape-html");
const sax = require("sax");
const https = require("https");

function modified(date) {
	return new Date(
		date.replace(/^(\d+)\.(\d+).(\d+) (\d+).(\d+) Uhr$/, "$3-$2-$1T$4:$5:00")
	);
}

function parser(payload, callbackRequest) {
	var curname, self;
	var p = sax
		.createStream(false, { lowercase: true })
		.on("opentag", function (node) {
			if (node.name.startsWith("edith:")) {
				curname = node.attributes["xmlns:edith"]
					? undefined
					: node.name.substr(6);
				payload[curname] = "";
			} else if (curname) {
				payload[curname] += `<${node.name}`;
				for (var a in node.attributes)
					payload[curname] += ` ${a}="escape(node.attributes[a])}"`;
				payload[curname] += `>`;
				self = node.isSelfClosing;
			}
		})
		.on("text", function (text) {
			if (curname) payload[curname] += escape(text);
		})
		.on("cdata", function (text) {
			if (curname) payload[curname] += escape(text);
		})
		.on("closetag", function (name) {
			if (name.startsWith("edith:")) curname = undefined;
			else if (curname) {
				if (self) self = false;
				else payload[curname] += `</${name}>`;
			}
		});
	return new Writable({
		write(chunk, encoding, callback) {
			p.write(chunk);
			callback();
		},
		final(callback) {
			p.end();
			callbackRequest(payload).then(function () {
				callback();
			}, this.destroy.bind(this));
		}
	});
}

function formparser(form) {
	return new Promise(function (resolve, reject) {
		var payload = {};
		var curname;
		sax
			.createStream(false, { lowercase: true })
			.on("opentag", function (node) {
				switch (node.name) {
					case "input":
						if (
							node.attributes.name &&
							((node.attributes.type !== "checkbox" &&
								node.attributes.type !== "radio") ||
								node.attributes.checked !== undefined)
						)
							payload[node.attributes.name] = node.attributes.value || "";
						break;
					case "textarea":
						if (node.attributes.name) payload[node.attributes.name] = "";
					case "select":
						if (node.attributes.name) curname = node.attributes.name;
						break;
					case "option":
						if (node.attributes.selected !== undefined) {
							payload[curname] = node.attributes.value;
							curname = undefined;
						}
						break;
				}
			})
			.on("text", function (text) {
				if (curname && payload[curname] !== undefined) payload[curname] += text;
			})
			.on("closetag", function (name) {
				switch (name) {
					case "select":
						payload[curname] = payload[curname] || "";
					case "textarea":
						curname = undefined;
						break;
				}
			})
			.on("end", function () {
				resolve(payload);
			})
			.on("error", reject)
			.end(form);
	});
}

function dirtree(resources, path, tree) {
	for (var d in tree) {
		var p = path + "/" + tree[d].name;
		resources[p] = new webdav.VirtualFileSystemResource({
			type: webdav.ResourceType.Directory,
			name: tree[d].name,
			props: { dir: tree[d].sys_pk }
		});
		if (tree[d].children) dirtree(resources, p, tree[d].children);
	}
}

class WebDAV extends webdav.VirtualFileSystem {
	constructor() {
		super();
		for (var d of ["/Medienarchiv", "/Aktuellmeldungen", "/Contentbausteine"])
			this.resources[d] = new webdav.VirtualFileSystemResource(
				webdav.ResourceType.Directory
			);
	}

	startup() {
		return credentials(process).then(
			function (username) {
				this.username = username;
			}.bind(this)
		);
	}

	requestOptions(method) {
		return {
			method,
			headers: {
				"accept-encoding": "br,gzip,deflate",
				cookie: session[this.username].cookie
			}
		};
	}

	_fastExistCheck(ctx, path, callback) {
		var superMethod = super._fastExistCheck.bind(this);
		var dir;
		if (
			path.toString() in this.resources ||
			(this.resources[(dir = path.getParent().toString())] &&
				this.resources[dir].props.listed)
		)
			superMethod(ctx, path, callback);
		else
			this._readDir(
				path.getParent(),
				{ context: ctx },
				function (err, children) {
					if (err) callback(false);
					else {
						/* this.resources[...] = undefined if we know it does not exist */
						this.resources[path.toString()] = this.resources[path.toString()];
						superMethod(ctx, path, callback);
					}
				}.bind(this)
			);
	}

	_readDir(path, ctx, callback) {
		var superMethod = super._readDir.bind(this);
		var p;
		var dir = path.toString();
		if (dir !== "/")
			for (var file in this.resources)
				if (
					file.startsWith(dir + "/") &&
					file.substr(dir.length + 1).indexOf("/") === -1 &&
					!this.resources[file]?.props.dir
				)
					delete this.resources[file];
		switch (dir) {
			case "/":
				p = Promise.resolve();
				break;
			case "/Medienarchiv":
				p = request(
					`/ekiba/media/d/2739?action=dir_getDirTree`,
					this.requestOptions("GET")
				).then(
					function ({ body }) {
						dirtree(this.resources, dir, JSON.parse(body).data.dirTree);
					}.bind(this)
				);
				break;
			case "/Aktuellmeldungen":
				p = request(
					`/ekiba/ajax/d/2856?action=dtGetRecords&con=aktuelluebersicht`,
					this.requestOptions("POST"),
					search(16, session[this.username].user)
				).then(
					function ({ body }) {
						JSON.parse(body).data.forEach(
							function (aktuell) {
								var name = `${aktuell[6].replace(/[/*]/g, "_")}.${aktuell[1]}.html`;
								this.resources[dir + "/" + name] =
									new webdav.VirtualFileSystemResource({
										type: webdav.ResourceType.File,
										size: -1,
										name: name,
										lastModifiedDate: modified(aktuell[13]),
										mimeType: "text/html",
										props: { id: aktuell[1] }
									});
							}.bind(this)
						);
					}.bind(this)
				);
				break;
			case "/Contentbausteine":
				p = request(
					`/ekiba/ajax/d/2997?action=dtGetRecords&con=contentcopyrefvorlagen`,
					this.requestOptions("POST"),
					{ ...search(5, "1,18"), ...search(12, session[this.username].user) }
				).then(
					function ({ body }) {
						JSON.parse(body).data.forEach(
							function (content) {
								var m = content[1].match(/^<a href="(.*?)"/);
								var name = `${content[4].replace(/[/*]/g, "_")}.${m[1]}.html`;
								this.resources[dir + "/" + name] =
									new webdav.VirtualFileSystemResource({
										type: webdav.ResourceType.File,
										size: -1,
										name: name,
										lastModifiedDate: modified(content[9]),
										mimeType: "text/html",
										props: { id: m[1], type: content[5] }
									});
							}.bind(this)
						);
					}.bind(this)
				);
				break;
			default:
				p = new Promise(
					function (resolve, reject) {
						this._fastExistCheck(
							ctx,
							path,
							function (exists) {
								var file;
								if (
									exists &&
									(file = this.resources[path.toString()]) &&
									file.props.dir
								)
									resolve(
										request(
											`/ekiba/media/d/2739?action=dir_getDirContent&dir_id=${file.props.dir}`,
											this.requestOptions("GET")
										).then(
											function ({ body }) {
												JSON.parse(body).data.dirContent.forEach(
													function (media) {
														media.variants.forEach(
															function ({
																sys_pk,
																name,
																fileinfo,
																upload_date,
																img_url
															}) {
																if (fileinfo.mime_type) {
																	if (media.variants.length > 1)
																		name = name.replace(
																			/\.(?=[^.]+$)/,
																			"." + sys_pk + "."
																		);
																	this.resources[dir + "/" + name] =
																		new webdav.VirtualFileSystemResource({
																			type: webdav.ResourceType.File,
																			size: -1,
																			name: name,
																			lastModifiedDate: new Date(
																				upload_date.replace(" ", "T")
																			),
																			mimeType: fileinfo.mime_type,
																			props: { id: sys_pk, src: img_url }
																		});
																}
															}.bind(this)
														);
													}.bind(this)
												);
											}.bind(this)
										)
									);
								else reject(webdav.Errors.ResourceNotFound);
							}.bind(this)
						);
					}.bind(this)
				);
		}
		p.then(
			function () {
				if (this.resources[dir]) this.resources[dir].props.listed = true;
				superMethod(path, ctx, callback);
			}.bind(this),
			function (err) {
				callback(err || webdav.Errors.Forbidden);
			}
		);
	}

	_create(path, ctx, callback) {
		if (path.rootName() === "Medienarchiv") super._create(path, ctx, callback);
		else callback(webdav.Errors.Forbidden);
	}

	_openReadStream(path, ctx, callback) {
		var p;
		var file = this.resources[path.toString()];
		if (file.type === webdav.ResourceType.Directory) p = Promise.reject();
		else
			switch (path.rootName()) {
				case "Medienarchiv":
					p = new Promise(function (resolve, reject) {
						https
							.get(file.props.src)
							.on("response", resolve)
							.on("error", reject);
					});
					break;
				case "Aktuellmeldungen":
					p = request(
						`/ekiba/edith/elem/2792/${file.props.id}?tab=aktuell_meldung_texte`,
						this.requestOptions("GET")
					).then(function ({ body }) {
						var payload = JSON.parse(
							Buffer.from(
								body.match(
									/<input .*?name="old_values_.*?" .*?value="(.*?)"/s
								)[1],
								"base64"
							)
						);
						return new PassThrough().end(`<edith:aktuell xmlns:edith="EDITH">
	<edith:titel>${unescape(payload.titel)}</edith:titel>
	<edith:untertitel>${unescape(payload.untertitel)}</edith:untertitel>
	<edith:zusammenfassung>${unescape(payload.zusammenfassung)}</edith:zusammenfassung>
	<edith:beschreibung>${unescape(payload.beschreibung)}</edith:beschreibung>
</edith:aktuell>`);
					});
					break;
				case "Contentbausteine":
					p = request(
						`/ekiba/edith/quick/2997/${file.props.id}`,
						this.requestOptions("GET")
					).then(function ({ body }) {
						switch (file.props.type) {
							case "HTML-Code":
								return new PassThrough().end(
									unescape(
										body.match(
											/<textarea-input .*?name="beschreibung2" .*?value="(.*?)"/s
										)[1]
									)
								);
							case "Text":
								var payload = JSON.parse(
									Buffer.from(
										body.match(
											/<input .*?name="old_values_.*?" .*?value="(.*?)"/s
										)[1],
										"base64"
									)
								);
								return new PassThrough()
									.end(`<edith:content xmlns:edith="EDITH">
	<edith:name1>${unescape(payload.name1)}</edith:name1>
	<edith:name1_format>${payload.name1_format}</edith:name1_format>
	<edith:name2>${unescape(payload.name2)}</edith:name2>
	<edith:name2_format>${payload.name2_format}</edith:name2_format>
	<edith:name3>${unescape(payload.name3)}</edith:name3>
	<edith:name_sichtbar_jn>${payload.name_sichtbar_jn}</edith:name_sichtbar_jn>
	<edith:beschreibung1>${unescape(payload.beschreibung1)}</edith:beschreibung1>
</edith:content>`);
						}
					});
					break;
				default:
					p = Promise.reject(webdav.Errors.ResourceNotFound);
			}
		p.then(callback.bind(undefined, null), function (err) {
			callback(err || webdav.Errors.Forbidden);
		});
	}

	_openWriteStream(path, ctx, callback) {
		var html, final;
		var file = this.resources[path.toString()];
		if (file?.type === webdav.ResourceType.Directory)
			return callback(webdav.Errors.Forbidden);
		if (path.rootName() === "Medienarchiv") {
			var dir = this.resources[path.getParent().toString()];
			if (!dir.props.dir) return callback(webdav.Errors.Forbidden);
			html = [];
		} else html = "";
		var opts = this.requestOptions("POST");
		switch (path.rootName()) {
			case "Medienarchiv":
				final = function () {
					var action, parnam, parval;
					if (html.length === 0) return Promise.resolve();
					if (file.props.id) {
						action = "updateVariantFile";
						parnam = "variant_id";
						parval = file.props.id;
					} else {
						action = "uploadFile";
						parnam = "dir_id";
						parval = dir.props.dir;
					}
					html.unshift(
						Buffer.from(`--edithdav
Content-Disposition: form-data; name="${parnam}"

${parval}
--edithdav
Content-Disposition: form-data; name="upload_name"

filedata
--edithdav
Content-Disposition: form-data; name="filedata"; filename="${path.fileName()}"
Content-Type: ${getType(path.fileName())}

`)
					);
					html.push(
						Buffer.from(`
--edithdav--
`)
					);
					opts.headers["content-type"] =
						"multipart/form-data; boundary=edithdav";
					return request(
						`/ekiba/media/d/2739?action=file_${action}`,
						opts,
						Buffer.concat(html)
					).then(function ({ body }) {
						body = JSON.parse(body);
						if (body.errors) throw errors;
						var upload = body.data.upload || body.data.file;
						file.props.id = upload.sys_pk;
						file.props.src = upload.img_url;
					});
				};
				break;
			case "Aktuellmeldungen":
				callback(
					null,
					parser(
						{
							sys_pk: file.props.id,
							speichereText: "Speichern"
						},
						(payload) =>
							request(
								`/ekiba/edith/elem/2792/${file.props.id}?tab=aktuell_meldung_texte&scene=aktuell_meldung_texte`,
								opts,
								payload
							)
					)
				);
				return;
			case "Contentbausteine":
				switch (file.props.type) {
					case "HTML-Code":
						final = function () {
							return request(
								`/ekiba/edith/quick/2997/${file.props.id}?tab=content_html`,
								opts,
								{ beschreibung2: html, speichereHTMLCode: "Speichern" }
							);
						};
						break;
					case "Text":
						callback(
							null,
							parser(
								{
									speichereTexte: "Speichern"
								},
								(payload) =>
									request(
										`/ekiba/edith/quick/2997/${file.props.id}?tab=content_texte`,
										opts,
										payload
									)
							)
						);
						return;
				}
				break;
			default:
				return callback(webdav.Errors.ResourceNotFound);
		}
		callback(
			null,
			new Writable({
				write(chunk, encoding, callback) {
					if (typeof html === "string") html += chunk.toString();
					else html.push(chunk);
					callback();
				},
				final(callback) {
					final().then(function () {
						callback();
					}, this.destroy.bind(this));
				}
			})
		);
	}

	_delete(path, ctx, callback) {
		var superMethod = super._delete.bind(this);
		var p;
		var file = this.resources[path.toString()];
		if (!file) p = Promise.reject(webdav.Errors.ResourceNotFound);
		else if (file.type === webdav.ResourceType.Directory) p = Promise.reject();
		else
			switch (path.rootName()) {
				case "Medienarchiv":
					p = request(
						`/ekiba/media/d/2739?action=dir_deleteFileVariants`,
						this.requestOptions("POST"),
						{ "file_id_list[]": file.props.id }
					);
					break;
				case "Aktuellmeldungen":
					p = request(
						`/ekiba/edith/elem/2792/${file.props.id}`,
						this.requestOptions("POST"),
						{ syspk: file.props.id, loescheEintrag: "Löschen" }
					);
					break;
				default:
					p = Promise.reject();
			}
		p.then(
			function () {
				superMethod(path, ctx, callback);
			},
			function (err) {
				callback(err || webdav.Errors.Forbidden);
			}
		);
	}
}

if (module.parent) module.exports = WebDAV;
else {
	var loader = new WebDAV();
	loader.startup().then(function () {
		var server = new webdav.WebDAVServer({
			hostname: "localhost",
			httpAuthentication: {
				askForAuthentication() {
					return [];
				},
				getUser(ctx, callback) {
					callback(null, { uid: loader.username, username: loader.username });
				}
			}
		});
		server.setFileSystemSync("/", loader);
		server.start(function (httpServer) {
			httpServer.on("request", function (req, res) {
				res.on("finish", function () {
					console.log(
						req.method,
						decodeURI(req.url),
						res.statusCode,
						res.statusMessage
					);
				});
			});
			console.log("WebDAV server running", httpServer.address());
		});
	});
}
